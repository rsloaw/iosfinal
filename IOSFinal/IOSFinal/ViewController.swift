//
//  ViewController.swift
//  IOSFinal
//
//  Created by Radosław on 16/06/2019.
//  Copyright © 2019 Radosław. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var levelLabel: UILabel!
    var buttonPressed = 0
    @IBOutlet weak var blueButton: UIButton!
    @IBAction func blueAction(_ sender: Any) {
        buttonPressed = 1

        
    }
    @IBOutlet weak var redButton: UIButton!
    @IBAction func redAction(_ sender: Any) {
        buttonPressed = 2

        
    }
    @IBOutlet weak var greenButton: UIButton!
    @IBAction func greenAction(_ sender: Any) {
        buttonPressed = 3
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        colorBack()

    }
   
    @IBAction func startButton(_ sender: Any) {
        level_1()
        
    }
    
    @objc func colorBack() {
        blueButton.backgroundColor = UIColor(red: 0, green: 0, blue: 1.0, alpha: 1.0);
        redButton.backgroundColor = UIColor(red: 1.0, green: 0, blue: 0, alpha: 1.0);
        greenButton.backgroundColor = UIColor(red: 0, green: 1.0, blue: 0, alpha: 1.0);
    }
    
    var checkInt=0
    func changeColor(){

        let randomInt = Int.random(in: 1..<4)
        if (randomInt == 1){
            checkInt = randomInt
            blueButton.backgroundColor = UIColor(red: 0, green: 0, blue: 1.0, alpha: 0.5)
            Timer.scheduledTimer(timeInterval: 0.2, target: self,selector: #selector(colorBack), userInfo: nil, repeats: false)
        }
        if (randomInt == 2){
            checkInt = randomInt
            redButton.backgroundColor = UIColor(red: 1.0, green: 0, blue: 0, alpha: 0.5)
            Timer.scheduledTimer(timeInterval: 0.2, target: self,selector: #selector(colorBack), userInfo: nil, repeats: false)

        }
        if (randomInt == 3){
            checkInt = randomInt
            greenButton.backgroundColor = UIColor(red: 0, green: 1.0, blue: 0, alpha: 0.5)
            Timer.scheduledTimer(timeInterval: 0.2, target: self,selector: #selector(colorBack), userInfo: nil, repeats: false)


        }
        
    }
    
    var score = 0
    func level_1(){
       Timer.scheduledTimer(withTimeInterval: 0.2, repeats: false) { timer in
            self.changeColor()
        }
  

        if (buttonPressed == checkInt){
            
            levelLabel.text = "Punkty: \(score)"
            score = score + 1
           
        } else  {
            score = 0
            
        }


}
    


}

